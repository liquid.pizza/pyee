import sys
import time
import os

import yeelight
from yeelight import Bulb
from yeelight import discover_bulbs

import json

# file name for the data file
dataFileName = "data.json"

# bulb dict
# keys are the names
# values are Bulb objects
bulbDict = {}

# dict
# keys are the names
# values are the IPs
nameIpDict = {}

def highLightBulb(bulb):
    print("[id] h")
    print("highlight bulb")
    bulb.turn_on()
    time.sleep(0.5)
    bulb.turn_off()
    time.sleep(0.5)
    bulb.turn_on()
    time.sleep(0.5)
    bulb.turn_off()

def highLightAll():
    print("h")
    print("highlight all bulbs")
    for bulb in bulbDict.values():
        highLightBulb(bulb)
        time.sleep(1)

def writeJson(data):
    with open(dataFileName, "w") as f:
        json.dump(data, f, indent=4, sort_keys=True)

def readJson():
    with open(dataFileName) as f:
        return json.load(f)

def updateBulbs():
    bulbs = discover_bulbs()
    nameIpDict = {bulb["capabilities"]["name"].lower(): bulb["ip"] for bulb in bulbs}
    writeJson(nameIpDict)
    return nameIpDict

def allBulbsOff():
    print("all bulbs off")
    for bulb in bulbDict.values():
        bulb.turn_off()

if (os.path.isfile(dataFileName)):
    print("file found")
    nameIpDict = readJson()
else:
    print("no file found")
    nameIpDict = updateBulbs()

if (len(nameIpDict) < 1):
    print("no bulbs found")
    quit()

bulbDict = {name.lower(): Bulb(ip, auto_on = True) for name, ip in nameIpDict.items()}
print(bulbDict)

if (len(sys.argv) > 1):

    if (str(sys.argv[1]) == "h"):
        highLightAll()
        quit()
    elif (str(sys.argv[1]) == "u"):
        print("update bulb list")
        print("u")
        bunameIpDictlbs = updateBulbs()
        bulbDict = {name: Bulb(ip) for name, ip in nameIpDict.items()}
        highLightAll()
        quit()
    elif (str(sys.argv[1]) == "off"):
        allBulbsOff()
        quit()
    elif (str(sys.argv[1]).lower() == "lampe"):
        bulbDict["lampe0"].toggle()
        bulbDict["lampe1"].toggle()
        quit()
    else:
        name = str(sys.argv[1]).lower()
        if (not bulbDict.get(name, None)):
            print("Bulb", name, "not found.")
            quit()
        print("found name", name)
        bulb = bulbDict[name]

    if (len(sys.argv)> 2):
        methode = str(sys.argv[2])
        params = sys.argv[3:]

        if (methode == "b"):
            print("set brightness")
            print("[id] b [brightness]")
            bulb.set_brightness(int(params[0]))
        elif (methode == "c"):
            print("set color")
            print("[id] c [r] [g] [b]")
            bulb.set_rgb(int(params[0]), int(params[1]), int(params[2]))
        elif (methode == "t"):
            print("set color temp")
            print("[id] t [color temp]")
            bulb.set_color_temp(int(params[0]))
        elif (methode == "s"):
            print("[id] s [time in s] [number of steps]")
            print("set sleep timer")
            sleep(int(params[0]), int(params[1]))
            print("sleep")
        elif (methode == "h"):
            highLightBulb(bulb)
    else:
        print("toggle bulb")
        bulb.toggle()
else:
    print("toggle bulb")
    bulb.toggle()
